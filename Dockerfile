FROM openjdk:17-jdk-alpine AS base
EXPOSE 9020

FROM gradle:jdk17 AS build
WORKDIR /src
COPY . .
RUN gradle build

FROM base AS final
WORKDIR "/app"
COPY --from=build /src/build/libs/*.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]
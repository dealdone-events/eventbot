package dealdone.events.services;

import dealdone.events.dao.UserRepository;
import dealdone.events.model.UserPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Component
public class AuthService {
    private final UserRepository userRepository;

    @Autowired
    public AuthService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public static URI getURI(long chatId) {
        return UriComponentsBuilder
                .fromPath("https://deals-done.ru/loginBot")
                .query("https://deals-done.ru/api/event-bot/auth&chatId=" + chatId)
                .build()
                .toUri();
    }

    public UserPojo saveUser(@RequestBody UserPojo usr) {
        return userRepository.save(usr);
    }
}

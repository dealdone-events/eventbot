package dealdone.events.dao;

import dealdone.events.model.UserPojo;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserPojo, Integer> {
}

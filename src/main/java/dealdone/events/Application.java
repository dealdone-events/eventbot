package dealdone.events;

import dealdone.events.bot.FinProEventsBot;
import dealdone.events.bot.TelegramConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

@Configuration
@EnableJpaRepositories
@SpringBootApplication
public class Application {
    public static void main(String[] args) throws TelegramApiException {
        var context = SpringApplication.run(Application.class, args);

        var config = context.getBean(TelegramConfig.class);
        TelegramBotsApi api = new TelegramBotsApi(DefaultBotSession.class);

        api.registerBot(new FinProEventsBot(config.getBotName(), config.getBotToken()));
    }
}

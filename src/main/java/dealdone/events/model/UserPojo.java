package dealdone.events.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "users")
public class UserPojo implements Serializable {
    @Id
    @GeneratedValue
    private Integer id;
    @JsonProperty("chatId")
    private Long chatId;
    @JsonProperty("token")
    private String token;

    public long getChatId() {
        return chatId;
    }

    public String getToken() {
        return token;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }
}

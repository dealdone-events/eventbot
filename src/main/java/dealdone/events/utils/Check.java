package dealdone.events.utils;

public class Check {
    public static<T> T checkIfNull(T obj) {
        if (obj == null) {
            throw new NullPointerException("Got null object.");
        }

        return obj;
    }
}

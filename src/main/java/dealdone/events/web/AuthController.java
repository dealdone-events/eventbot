package dealdone.events.web;

import dealdone.events.model.UserPojo;
import dealdone.events.services.AuthService;
import dealdone.events.utils.Check;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@RestController
@RequestMapping("/response")
public class AuthController {
    private final AuthService authService;

    public AuthController(AuthService authService){
        this.authService = authService;
    }

    @PostMapping("/getresponsebody")
    public void registerUser(@RequestBody UserPojo usr) {
        Check.checkIfNull(usr);
        authService.saveUser(usr);
    }
}

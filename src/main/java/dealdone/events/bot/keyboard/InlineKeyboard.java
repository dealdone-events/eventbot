package dealdone.events.bot.keyboard;

import dealdone.events.bot.callback.CallbackData;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

public class InlineKeyboard {
    public  InlineKeyboardMarkup getInlineButtons() {
        List<List<InlineKeyboardButton>> buttons = new ArrayList<>();

        buttons.add(getButton("Описание", CallbackData.INFO.name()));
        buttons.add(getButton("Текущие значения", CallbackData.RATES.name()));

        var inlineKeyboardMarkup = new InlineKeyboardMarkup();
        inlineKeyboardMarkup.setKeyboard(buttons);

        return inlineKeyboardMarkup;
    }

    public List<InlineKeyboardButton> getButton(String title, String callBackData) {
        var button = new InlineKeyboardButton();
        button.setText(title);
        button.setCallbackData(callBackData);

        List<InlineKeyboardButton> buttonRow = new ArrayList<>();
        buttonRow.add(button);

        return buttonRow;
    }
}

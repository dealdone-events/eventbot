package dealdone.events.bot.callback;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;

import java.io.IOException;

public class CallbackHandler {
    public BotApiMethod<?> handleCallback(CallbackQuery callbackQuery) throws IOException {
        Long chatId = callbackQuery.getMessage().getChatId();
        String data = callbackQuery.getData();

        if (data.equals(CallbackData.INFO.name())) {
            return getInfo(chatId);
        } else {
            return getRates(chatId);
        }
    }

    private SendMessage getInfo(Long chatId) {
        // todo
        return null;
    }

    private SendMessage getRates(Long chatId) {
        // todo
        return null;
    }
}

package dealdone.events.bot.message;

import dealdone.events.bot.keyboard.InlineKeyboard;
import dealdone.events.services.AuthService;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.BotApiMethod;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;

public class MessageHandler {
    private final InlineKeyboard keyboard = new InlineKeyboard();

    public BotApiMethod<?> handleMessage(Message message) throws IllegalArgumentException {
        Long chatId = message.getChatId();
        String messageText = message.getText();

        return switch (messageText) {
            case "/start" -> SendMessage.builder()
                    .text(BotMessage.START.getMessage() + AuthService.getURI(chatId))
                    .chatId(chatId.toString())
                    .build();
            case "/help" -> SendMessage.builder()
                    .text(BotMessage.HELP.getMessage())
                    .chatId(chatId.toString())
                    .build();
            case "/get_day" -> SendMessage.builder()
                    .text(BotMessage.GET_DAY.getMessage())
                    .replyMarkup(keyboard.getInlineButtons())
                    .chatId(chatId.toString())
                    .build();
            case "/get_week_highs" -> SendMessage.builder()
                    .text(BotMessage.GET_WEEK_HIGHS.getMessage())
                    .replyMarkup(keyboard.getInlineButtons())
                    .chatId(chatId.toString())
                    .build();
            default -> new SendMessage(chatId.toString(), BotMessage.INCORRECT_INPUT.getMessage());
        };
    }
}

package dealdone.events.bot;

import dealdone.events.bot.callback.CallbackHandler;
import dealdone.events.bot.message.MessageHandler;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.io.IOException;

public class FinProEventsBot extends TelegramLongPollingBot {
    private final String botName;
    private final String botToken;
    private final MessageHandler messageHandler = new MessageHandler();
    private final CallbackHandler callbackHandler = new CallbackHandler();

    public FinProEventsBot(String botName, String botToken) {
        this.botName = botName;
        this.botToken = botToken;
    }

    @Override
    public void onUpdateReceived(Update update) {
        try {
            if (update.hasCallbackQuery()) {
                var callBackQuery = update.getCallbackQuery();
                this.execute(callbackHandler.handleCallback(callBackQuery));
            } else {
                this.execute(messageHandler.handleMessage(update.getMessage()));
            }
        } catch (IOException ex) {
            // todo add logging
        } catch (TelegramApiException ex) {
            // todo add logging
        }
    }

    @Override
    public String getBotUsername() {
        return botName;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }
}
